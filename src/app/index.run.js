(function() {
  'use strict';

  angular
    .module('bmu')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
