(function() {
  'use strict';

  angular
    .module('bmu')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('login', {
        url: '/login',
        templateUrl: 'app/main/login.tpl.html'
      });

    $urlRouterProvider.otherwise('/login');
  }

})();
