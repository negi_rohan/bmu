/* global moment:false */
(function() {
  'use strict';

  angular
    .module('bmu')
    .constant('moment', moment);
})();
