(function() {
  'use strict';

  angular
    .module('bmu', [
    	'ngCookies',
    	'ngAria',
    	'ui.router',
    	'ui.bootstrap',
    	'toastr',
    	'mgcrea.ngStrap'
	]);

})();
